# Bunk Bed Injuries 

A quick data dive to see what are the most common injuries sustained around bunk beds. 

Data source: https://www.cpsc.gov/Research--Statistics/NEISS-Injury-Data

### [Demo of workbook](http://nbviewer.jupyter.org/urls/gitlab.com/cryocaustik/bunkbed_injuries/raw/f54d1bab8a8cc1244c0b72ab05f85db0f3cccea5/bunkbed_injuries.ipynb)